<?php

	require_once dirname(__FILE__) . '/../vendor/autoload.php';
	require_once dirname(__FILE__) . '/credentials.php';

	if (isset($_POST["desc"]) && isset($_POST["duration"])) {
		$desc = $_POST["desc"];
		$duration = $_POST["duration"];

		if (!empty($desc) && !empty($duration)) {

			// Convert (h m) format into seconds

			$hours = 0;
			$pos_h = strpos($duration, 'h', 0);
			if ($pos_h !== false) {
				$hours = intval(substr($duration, 0, $pos_h));
			}

			$minutes = 0;
			$pos_m = strpos($duration, 'm', 0);
			if ($pos_m !== false) {
				if ($pos_h !== false) {
					$minutes = intval(substr($duration, $pos_h+1, $pos_m-$pos_h-1));
				} else {
					$minutes = intval(substr($duration, 0, $pos_m));
				}
			}
			$duration = $hours*60 + $minutes;

			DB::insert("log", array(
				'desc' => $desc,
				'duration' => $duration
			));

		} else {
			// do nothing, since the user-friendly validation is implemented in JQuery on the client
		}
	}
?>