<?php

	require_once '../vendor/autoload.php';
	require_once dirname(__FILE__) . '/credentials.php';

	function getLogs() {	

		return DB::query("SELECT * FROM log ORDER BY timestamp DESC");
	}

?>