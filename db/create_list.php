<?php

	require_once dirname(__FILE__) . '/../vendor/autoload.php';
	require_once dirname(__FILE__) . '/credentials.php';

	$LOGS_PER_PAGE = 10;

	$logs = getLogs();

	$strana = new \Strana\Paginator();
	$paginator = $strana->perPage($LOGS_PER_PAGE)->make($logs);
	$lastDate = null;
	$res = '';
	foreach ($paginator as $log) {

		$thisDate = (new DateTime($log['timestamp']))->format('Y-m-d');
		if (!isset($lastDate)){ // if the first log entry
			$res .= startDay($thisDate);
			$res .= outputLog($log);
		} else { // if not the first log entry
			if ($lastDate == $thisDate) { // if the same date
				// Continue the previous day
				$res .= outputLog($log);
			} else { // if not the same date
				// Start a new day
				$res .= endDay();
				$res .= startDay($thisDate);
				$res .= outputLog($log);
			}
		}
		$lastDate = $thisDate;
	}
	$res .= endDay();

	echo $res . $paginator;

	function getLogs() {

		return DB::query("SELECT * FROM log ORDER BY timestamp DESC");
	}

	function minutesToString($minutes) {
		$hours = 0;
		if ($minutes >= 60) {
				$hours = (int)($minutes / 60);
				$minutes = $minutes % 60;
		}
		return $hours . 'h ' . $minutes . 'm';
	}

	function startDay($date) {
		if ($date == (new DateTime())->format('Y-m-d')) { // if this log entry was logged today
			$date = 'Today';
		}
		return "
			<div class='areaTitle'>" . $date . "</div>
			<table>
			<tr class='caption'><td>Description</d><td>Time spent</td><td>Date</td></tr>

		";
	}

	function endDay() {
		return "
			</table><br>
		";
	}

	function outputLog($log) {
		return "
			<tr>
				<td class='tdDesc'>" . $log['desc'] . "</td>
				<td class='tdDuration'>" . minutesToString($log['duration']) . "</td>
				<td class='tdTimestamp'>" . $log['timestamp'] . "</td>
			</tr>
		";
	}

?>