<!DOCTYPE html>

<html>
	<head>
		<link rel="stylesheet" type="text/css" href="css/styles.css">
		<title>
			Loggio
		</title>
	</head>

	<body>

		<div id="header">
			<div id="logo">Loggio</div>
		</div>
		<div id="main">
			<div id="content">

				<div id="logList">

					<script src="http://code.jquery.com/jquery-1.11.3.min.js"></script>

					<?php
						require_once 'db/create_list.php';
					?>
					
				</div>
			</div>
			<div id="rightbar">
				
				<div id="addLog">
					<form id="formLogAdd" method="post", action="">
						<fieldset>
							<legend><div class='areaTitle'>Add a new time log</div></legend>
							<p>
								<label class="formLabel" for="desc">Description <span class="comment">(required)</span></label><br>
								<input class="formText" type="text" id="desc" minlength="2" required>
							</p>
							<p>
								<label class="formLabel" for="duration">Time spent <span class="comment">(required, [XXh] [XXm])</span></label><br>
								<input class="formText" type="text" id="duration" name="duration" required>
							</p>
							<p>
								<input type="submit" id="log-add" value="Add">
							</p>
							<div id="result"></div>
						</fieldset>
					</form>

					<script src="js/add_log.js"></script>
					<script src="js/jquery.validate.js"></script>

					<script type="text/javascript">

						$(document).ready(function() {

							$.validator.addMethod(
								"verifyDuration", function(value, element, regexp) {
									var re = new RegExp(regexp);
									return this.optional(element) || re.test(value);
								},
								"Please check your input."
							);

							$("#formLogAdd").validate({
								rules : {
									duration: {
										verifyDuration: "^([0-9]{1,2}h){0,1}( ){0,1}([0-9]{1,2}m){0,1}$"
									}
								},
								errorElement: 'div',
								submitHandler: function () {
								    processForm();
								}
							});

						});
					</script>

				</div>
			</div>
		</div>
		<div id="footer">
			Job interview test task for <a href="http://www.scandiweb.com/">Scandiweb</a> by <a href="mailto:ilja.astahovs@google.com">Ilja Astahovs</a> , 2015.<br>
			Written in PHP 5.4.22, jQuery 1.11.3, HTML, CSS, for MySQL 5.5.34, XAMPP 1.8.22. Tested on Google Chrome 46.0.2490 m.
		</div>

	</body>

</html>