function processForm() {

	var desc = $("input#desc").val();
	var duration = $("input#duration").val();
	if (($.trim(desc) != '') && ($.trim(duration) != '')){

		$.ajax({
			method: "POST",
			url: "db/add_log.php",
			data: {desc: desc, duration: duration}
		}).done(
			function(res) {
				$.ajax({
					method: "POST",
					url: "db/create_list.php",
					data: {}
				}).done(
					function(list) {
						$("div#logList").html(list);
						$("input#desc").val('');
						$("input#duration").val('');
					}
				)
			}
		);
	};
}