-- phpMyAdmin SQL Dump
-- version 4.0.9
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Nov 04, 2015 at 10:39 PM
-- Server version: 5.5.34
-- PHP Version: 5.4.22

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `scandiweb_logger`
--

-- --------------------------------------------------------

--
-- Table structure for table `log`
--

CREATE TABLE IF NOT EXISTS `log` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `desc` varchar(255) NOT NULL,
  `duration` int(11) NOT NULL,
  `timestamp` timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=240 ;

--
-- Dumping data for table `log`
--

INSERT INTO `log` (`id`, `desc`, `duration`, `timestamp`) VALUES
(1, 'Went to gym', 120, '2015-11-04 15:59:06'),
(2, 'Slept', 450, '2015-11-04 15:58:02'),
(3, 'Took a walk', 45, '2015-11-02 20:14:32'),
(16, 'Worked', 550, '2015-11-03 10:28:01'),
(48, 'Red a book', 65, '2015-11-03 13:31:21'),
(182, 'Laughed', 10, '2015-11-04 16:00:01'),
(183, 'Danced', 5, '2015-11-04 16:09:42'),
(227, 'Googled', 10, '2015-11-04 19:03:28'),
(237, 'Wrote a mail', 15, '2015-11-04 21:36:56'),
(238, 'Cooked dinner', 35, '2015-11-04 21:37:20'),
(239, 'Created a remote repo', 5, '2015-11-04 21:37:49');

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
